package com.cgycms.prototype.common;

/**
 * @author: 超哥呦
 * @Description: 通用controller
 * @date: 2021/11/18 20:47
 **/
public class BaseController {


    /**
     * controller 统一返回
     *
     * @return
     */
    public Result success() {
        return Result.success();
    }

    public Result success(Object data) {
        return Result.success(data);
    }

    public Result error(String msg) {
        return Result.error(msg);
    }

    public Result error(Object data) {
        return Result.error(data);
    }


}

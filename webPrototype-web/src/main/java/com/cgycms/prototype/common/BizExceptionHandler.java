package com.cgycms.prototype.common;

import cn.dev33.satoken.exception.NotLoginException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @ClassName BizExceptionHandler
 * @Description 异常统一处理
 * @Author 超哥呦
 * @Date 2021/11/25 22:28
 **/
@RestControllerAdvice
public class BizExceptionHandler {

    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 处理 Exception 异常
     *
     * @param e 异常
     * @return 处理结果
     */
    @ExceptionHandler(Exception.class)
    public Result handlerException(Exception e) {
        logger.error(e.getMessage(), e);
        return Result.errorDetail(e.getMessage());
    }

    /**
     * 处理空指针异常
     *
     * @param e 异常
     * @return 处理结果
     */
    @ExceptionHandler(NullPointerException.class)
    public Result handlerNullPointerException(NullPointerException e) {
        logger.error(e.getMessage(), e);
        return Result.error("空指针异常");
    }

    /**
     * 处理自定义异常
     *
     * @param e 异常
     * @return 处理结果
     */
    @ExceptionHandler(BizException.class)
    public Result handlerGlobalException(BizException e) {
        logger.error(e.getMessage(), e);
        return Result.error(e.getCode(), e.getMessage());
    }

    /**
     * 处理登录异常
     *
     * @param e 异常
     * @return 处理结果
     */
    @ExceptionHandler(NotLoginException.class)
    public Result handlerGlobalException(NotLoginException e) {
        logger.error(e.getMessage(), e);
        return Result.errorDetail(401,"登录信息过期,请您重新登录!",e.getMessage());
    }

}

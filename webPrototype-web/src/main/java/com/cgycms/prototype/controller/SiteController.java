package com.cgycms.prototype.controller;

import cn.hutool.core.lang.hash.Hash;
import com.cgycms.prototype.SitePrototypeService;
import com.cgycms.prototype.cache.TimeCacheUtils;
import com.cgycms.prototype.common.BaseController;
import com.cgycms.prototype.common.Result;
import com.cgycms.prototype.constant.ServiceCacheKeyConstant;
import com.cgycms.prototype.entity.SiteEntity;
import com.cgycms.prototype.util.UserUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: 超哥呦
 * @Description: 站点信息模块
 * @date: 2021/11/14 0:00
 **/
@Api(tags = "站点管理相关模块")
@Controller
public class SiteController extends BaseController {

    @Autowired
    private SitePrototypeService sitePrototypeService;


    @ApiOperation(value = "查询当前站点信息配置")
    @GetMapping("/querySite")
    @ResponseBody
    public Result query() {
        SiteEntity siteEntity = sitePrototypeService.querySite();
        return success(siteEntity);
    }

    @ApiOperation(value = "清空所有缓存信息")
    @GetMapping("/clearCache")
    @ResponseBody
    public Result clearCache() {
        TimeCacheUtils.clear();
        return success();
    }

    @ApiOperation(value = "修改保存项目配置信息")
    @PostMapping("/updateSite")
    @ResponseBody
    public Result updateSite(@RequestBody SiteEntity siteEntity) {
        if (!UserUtils.isAdmin()) {
            return error("抱歉，您不是超级管理员无权修改！");
        }
        siteEntity.setId(1L);
        sitePrototypeService.updateById(siteEntity);
        TimeCacheUtils.remove(ServiceCacheKeyConstant.SITE_CACHE_KEY);
        return success();
    }

    @ApiOperation(value = "系统初始化配置")
    @GetMapping("/initSite")
    @ResponseBody
    public Result initSite() {
        SiteEntity siteEntity = sitePrototypeService.querySite();
        Map<String,Object> param = new HashMap<>();
        param.put("siteName",siteEntity.getSiteName());
        param.put("siteLogo",siteEntity.getSiteLogo());
        param.put("hostName",siteEntity.getHostName());
        return success(param);
    }

    /**
     * 跳转首页
     * @return
     */
    @RequestMapping(path = {"/" , "/index" , "/home"})
    public String index() {
        return "index";
    }


}

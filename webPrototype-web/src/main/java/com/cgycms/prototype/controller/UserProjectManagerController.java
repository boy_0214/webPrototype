package com.cgycms.prototype.controller;

import cn.hutool.core.util.ObjectUtil;
import com.cgycms.prototype.UserProjectManagerService;
import com.cgycms.prototype.common.BaseController;
import com.cgycms.prototype.common.Result;
import com.cgycms.prototype.model.UserProjectDetailModel;
import com.cgycms.prototype.model.UserProjectModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * @author: 超哥呦
 * @Description: 用户管理项目控制器
 * @date: 2021/11/18 20:19
 **/
@Api(tags = "用户项目管理模块")
@RestController
@RequestMapping("/userManager")
public class UserProjectManagerController extends BaseController {

    @Autowired
    private UserProjectManagerService userProjectManagerService;


    /**
     * 用户创建项目信息
     *
     * @param model 实体
     * @return
     */
    @ApiOperation(value = "用户创建项目信息")
    @PostMapping("/createProject")
    public Result createProject(@RequestBody UserProjectModel model) {
        String msg = verificationParam(model);
        if (ObjectUtil.isNotNull(msg)) {
            return error(msg);
        }
        return success(userProjectManagerService.createProject(model));
    }


    /**
     * 项目项目列表
     *
     * @param name 模糊搜索项目名称
     * @param type 0 我的项目 1公开的项目
     * @return
     */
    @ApiOperation(value = "用户查询项目列表", response = UserProjectModel.class)
    @ApiImplicitParams({
            @ApiImplicitParam(value = "根据项目名称模糊搜索", name = "name", dataType = "String", required = false),
            @ApiImplicitParam(value = "查询类型 0查询自己的项目,1查询其他人的项目", name = "type", dataType = "Integer", required = true)
    })
    @GetMapping("/queryProjectList")
    public Result queryProjectList(@RequestParam(value = "name", required = false) String name, @RequestParam(value = "type") Integer type) {
        List typeList = Arrays.asList(0, 1);
        if (!typeList.contains(type)) {
            return error("查询参数非法！");
        }
        return success(userProjectManagerService.queryProjectList(name, type));
    }


    /**
     * 查询项目详情
     *
     * @param id 项目的主键ID
     * @return
     */
    @ApiOperation(value = "用户查询项目详情信息", response = UserProjectDetailModel.class)
    @ApiImplicitParams({
            @ApiImplicitParam(value = "项目的主键ID", name = "id", dataType = "Long", required = true),
    })
    @GetMapping("/queryProjectById")
    public Result queryProjectById(@RequestParam(value = "id") Long id) {
        if (ObjectUtil.isNull(id)) {
            return error("必填参数为空！");
        }
        return success(userProjectManagerService.queryProjectById(id));
    }


    /**
     * 删除项目信息及原型文件接口
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "删除项目信息及原型文件接口")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "主键ID", name = "id", dataType = "Long", required = true)
    })
    @GetMapping("/projectDelete")
    public Result projectDelete(@RequestParam Long id) {
        if (ObjectUtil.isEmpty(id)) {
            return error("参数不能为空！");
        }
        return success(userProjectManagerService.projectDelete(id));
    }


    /**
     * 用户修改项目详情信息
     *
     * @param model 实体
     * @return
     */
    @ApiOperation(value = "用户修改项目详情信息")
    @PostMapping("/updateProject")
    public Result updateProject(@RequestBody UserProjectModel model) {
        String msg = verificationParam(model);
        if (ObjectUtil.isNull(model.getId())) {
            return error("项目ID不能为空！");
        }
        if (ObjectUtil.isNotNull(msg)) {
            return error(msg);
        }
        return success(userProjectManagerService.updateProject(model));
    }


    private String verificationParam(UserProjectModel model) {
        if (ObjectUtil.isNull(model)) {
            return "必填参数为空！";
        }
        if (ObjectUtil.isEmpty(model.getProjectName())) {
            return "项目名称不能为空！";
        }
        if (ObjectUtil.isEmpty(model.getPassType())) {
            return "请选择是否可以匿名访问！";
        }
        if (1 == model.getPassType() && ObjectUtil.isEmpty(model.getProjectPassword())) {
            return "请输入预览访问密码！";
        }
        if (ObjectUtil.isEmpty(model.getToOpen())) {
            return "请选择是否公开项目！";
        }
        return null;
    }

}

package com.cgycms.prototype.controller;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.cgycms.prototype.UserService;
import com.cgycms.prototype.cache.TimeCacheUtils;
import com.cgycms.prototype.common.BaseController;
import com.cgycms.prototype.common.Result;
import com.cgycms.prototype.constant.ServiceCacheKeyConstant;
import com.cgycms.prototype.entity.LoginUserEntity;
import com.cgycms.prototype.model.UserModel;
import com.cgycms.prototype.model.UserRegisterModel;
import com.cgycms.prototype.model.UserUpdateModel;
import com.cgycms.prototype.util.UserUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @ClassName UserLoginController
 * @Description 用户登录模块
 * @Author 超哥呦
 * @Date 2021/11/27  15:15
 **/
@Api(tags = "系统用户登录模块")
@RestController
@RequestMapping("/userManager")
public class UserLoginController extends BaseController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "用户注册新增接口")
    @PostMapping("/registerUser")
    public Result registerUser(@RequestBody UserRegisterModel userRegisterModel) {
        if (ObjectUtil.isNull(userRegisterModel)) {
            return error("必填参数为空!");
        }

        if (ObjectUtil.isEmpty(userRegisterModel.getNickName())) {
            return error("用户名称不能为空!");
        }

        if (ObjectUtil.isEmpty(userRegisterModel.getLoginName())) {
            return error("用户登录账号不能为空!");
        }

        if (ObjectUtil.isEmpty(userRegisterModel.getPassword())) {
            return error("用户密码不能为空!");
        }

        if (!UserUtils.isAdmin()) {
            return error("抱歉，您不是超级管理员无法添加用户！");
        }

        if (userService.selectUserByLoginNameCount(userRegisterModel.getLoginName()) > 0) {
            return error("当前登录账号已经存在，请更换再试！");
        }

        LoginUserEntity loginUserEntity = new LoginUserEntity();
        BeanUtils.copyProperties(userRegisterModel, loginUserEntity);
        String password = SecureUtil.md5(userRegisterModel.getPassword() + userRegisterModel.getLoginName());
        loginUserEntity.setPassword(password);
        loginUserEntity.setCreateTime(DateUtil.date());
        userService.save(loginUserEntity);
        TimeCacheUtils.remove(ServiceCacheKeyConstant.SYS_USERS_ALL);
        return success(loginUserEntity.getId());
    }

    @ApiOperation(value = "用户登录接口")
    @PostMapping("/doLogin")
    public Result doLogin(@RequestParam String username, @RequestParam String password) {
        if (ObjectUtil.isEmpty(username) || ObjectUtil.isEmpty(password)) {
            return error("账号或密码不能为空!");
        }
        return success(userService.doLogin(username, password));
    }

    @ApiOperation(value = "用户是否登录接口")
    @GetMapping("isLogin")
    public Result isLogin() {
        return success(StpUtil.isLogin());
    }

    @ApiOperation(value = "用户信息查询接口")
    @GetMapping("/userDetail")
    public Result userDetail() {
        return success(UserUtils.getUser());
    }

    @ApiOperation(value = "用户退出登录接口")
    @GetMapping("/loginOut")
    public Result loginOut() {
        return success(UserUtils.kicking(null));
    }

    @ApiOperation(value = "查询所有用户列表")
    @GetMapping("/queryUserList")
    public Result queryUserList() {
        if (!UserUtils.isAdmin()) {
            return error("抱歉，您不是超级管理员无权查询！");
        }
        return success(userService.queryUserList());
    }

    @ApiOperation(value = "批量删除用户信息")
    @PostMapping("/deleteBatchUser")
    public Result deleteBatchUser(@RequestBody List<Long> ids) {
        if (!UserUtils.isAdmin()) {
            return error("抱歉，您不是超级管理员无权删除！");
        }
        if (ObjectUtil.isEmpty(ids)) {
            return error("必填参数为空！");
        }
        TimeCacheUtils.remove(ServiceCacheKeyConstant.SYS_USERS_ALL);
        return success(userService.deleteBatchUser(ids));
    }

    @ApiOperation(value = "修改用户的昵称或密码")
    @PostMapping("/updateUserModel")
    public Result updateUser(@RequestBody UserUpdateModel userModel) {
        if (ObjectUtil.isNull(userModel)) {
            return error("必填参数为空！");
        }
        if (ObjectUtil.isNull(userModel.getId())) {
            return error("用户ID不能为空!");
        }
        if (ObjectUtil.isNull(userModel.getPassword()) && ObjectUtil.isNull(userModel.getNickName())) {
            return error("请输入要修改的密码或昵称！");
        }
        if (StrUtil.isNotBlank(userModel.getPassword())) {
            Map<Long, UserModel> longUserModelMap = userService.queryUserMap();
            UserModel userModelById = longUserModelMap.get(userModel.getId());
            String password = SecureUtil.md5(userModel.getPassword() + userModelById.getLoginName());
            userModel.setPassword(password);
        } else {
            userModel.setPassword(null);
        }
        if(StrUtil.isBlank(userModel.getNickName())){
            userModel.setNickName(null);
        }
        if (UserUtils.isAdmin()) {
            LoginUserEntity userEntity = new LoginUserEntity();
            BeanUtils.copyProperties(userModel, userEntity);
            userService.updateById(userEntity);
        } else {
            if (!userModel.getId().equals(UserUtils.getUser().getId())) {
                return error("您无权操作，请联系管理员！");
            }
            LoginUserEntity userEntity = new LoginUserEntity();
            BeanUtils.copyProperties(userModel, userEntity);
            userService.updateById(userEntity);
        }
        UserUtils.kicking(userModel.getId());
        TimeCacheUtils.remove(ServiceCacheKeyConstant.SYS_USERS_ALL);
        return success();
    }
}

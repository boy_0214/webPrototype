package com.cgycms.prototype.common;

import lombok.Data;

/**
 * @ClassName BizException
 * @Description 统一异常类
 * @Author 超哥呦
 * @Date 2021/11/25  22:23
 **/
@Data
public class BizException extends RuntimeException {

    private Integer code;

    private String msg;

    public BizException(String msg) {
        super(msg);
        this.msg = msg;
        this.code = 500;
    }


    public BizException(String msg, Throwable e) {
        super(msg, e);
        this.msg = msg;
    }


    public BizException(String msg, Integer code, Throwable e) {
        super(msg, e);
        this.msg = msg;
        this.code = code;
    }
}

package com.cgycms.prototype.common;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: 超哥呦
 * @Description: 统一响应类
 * @date: 2021/11/18 20:40
 **/
@Api(value = "统一响应类")
@Data
public class Result<T> {

    @ApiModelProperty(value = "响应状态码")
    private Integer code;

    @ApiModelProperty(value = "提示信息")
    private String msg;

    @ApiModelProperty(value = "错误异常信息")
    private String detailMsg;

    @ApiModelProperty(value = "实体数据")
    private T data;

    /*****               统一返回前端对象               *****/
    public static Result success() {
        Result result = new Result();
        result.setCode(200);
        result.setMsg("操作成功！");
        return result;
    }

    public static Result success(Object data) {
        Result result = new Result();
        result.setCode(200);
        result.setMsg("操作成功！");
        result.setData(data);
        return result;
    }

    public static Result success(String msg) {
        Result result = new Result();
        result.setCode(200);
        result.setMsg(msg);
        return result;
    }

    public static Result error() {
        Result result = new Result();
        result.setCode(500);
        result.setMsg("操作失败！");
        return result;
    }

    public static Result error(Object data) {
        Result result = new Result();
        result.setCode(500);
        result.setMsg("操作失败！");
        result.setData(data);
        return result;
    }

    public static Result error(String msg) {
        Result result = new Result();
        result.setCode(500);
        result.setMsg(msg);
        return result;
    }

    public static Result error(Integer code, String msg) {
        Result result = new Result();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    public static Result errorDetail(String msg) {
        Result result = new Result();
        result.setCode(500);
        result.setMsg("操作失败！");
        result.setDetailMsg(msg);
        return result;
    }

    public static Result errorDetail(String msg,String detailMsg) {
        Result result = new Result();
        result.setCode(500);
        result.setMsg(msg);
        result.setDetailMsg(detailMsg);
        return result;
    }

    public static Result errorDetail(Integer code,String msg,String detailMsg) {
        Result result = new Result();
        result.setCode(code);
        result.setMsg(msg);
        result.setDetailMsg(detailMsg);
        return result;
    }
}

package com.cgycms.prototype.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName UserProjectEntity
 * @Description 项目信息表
 * @Author 超哥呦
 * @Date 2021/11/24 0024 23:42
 **/
@ToString
@Data
@TableName("cgycms_project")
public class UserProjectEntity extends Model<UserProjectEntity> implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId
    private Long id;

    /**
     * 项目唯一编码
     */
    private String projectCode;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 描述
     */
    private String projectDesc;

    /**
     * 是否公开 1公开 0不公开
     */
    private Integer toOpen;

    /**
     * 最新原型版本
     */
    private Integer projectVersion;

    /**
     * 访问密码
     */
    private String projectPassword;

    /**
     * 是否匿名访问 0可以 1输入密码
     */
    private Integer passType;

    /**
     * 预览地址
     */
    private String previewUrl;

    /**
     * 创建人id
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人id
     */
    private String updateBy;

    /**
     * 修改时间
     */
    private Date updateTime;
}

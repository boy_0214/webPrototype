package com.cgycms.prototype.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author: 超哥呦
 * @Description: 站点配置对象
 * @date: 2021/11/13 23:20
 **/
@ToString
@TableName("cgycms_site")
@Data
public class SiteEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId
    private Long id;

    /**
     * 站点名称
     */
    private String siteName;

    /**
     * 存储路径
     */
    private String diskName;

    /**
     * 预览路径
     */
    private String previewName;

    /**
     * 是否开启登陆控制
     */
    private Integer roleSwitch;

    /**
     * 站点路径
     */
    private String hostName;

    /**
     * 是否开启云储存
     */
    private Integer ossSwitch;

    /**
     * 是否开启版本记录控制
     */
    private Integer dataVersion;

    /**
     * 原型解压输出路径
     */
    private String zipOutName;

    private String siteLogo;
}

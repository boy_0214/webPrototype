package com.cgycms.prototype.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: 超哥呦
 * @Description: 原型记录实体
 * @date: 2021/11/18 20:33
 **/
@ToString
@Data
@TableName("cgycms_prototype_file")
public class PrototypeFileEntity extends Model<PrototypeFileEntity> implements Serializable  {

    private static final long serialVersionUID = 1L;

    @TableId
    private Long id;

    /**
     * 上传时文件名称
     */
    private String originalName;

    /**
     * 新的文件名称
     */
    private String fileNewName;

    /**
     * 存储位置
     */
    private String diskPath;

    /**
     * 解压存储路径
     */
    private String unzipPath;

    /**
     * 预览地址
     */
    private String previewUrl;

    /**
     * 上传时文件类型
     */
    private String uploadFileType;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 所属项目编码
     */
    private String code;

    /**
     * 版本号
     */
    private Integer version;

    /**
     * 压缩包大小byte
     */
    private Long zipSize;

    /**
     * 解压后目录大小byte
     */
    private Long unzipSize;

    /**
     * 项目id
     */
    private Long projectId;
}

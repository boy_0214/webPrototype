package com.cgycms.prototype.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName LoginUserEntity
 * @Description 用户表
 * @Author 超哥呦
 * @Date 2021/11/27 21:39
 **/
@Data
@TableName("cgycms_users")
public class LoginUserEntity extends Model<LoginUserEntity> implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId
    private Long id;

    private String nickName;

    private String loginName;

    private String password;

    private Integer state;

    private Date createTime;

    private Date updateTime;

}

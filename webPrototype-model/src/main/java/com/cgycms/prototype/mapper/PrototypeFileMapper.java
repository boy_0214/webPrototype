package com.cgycms.prototype.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cgycms.prototype.entity.PrototypeFileEntity;
import com.cgycms.prototype.model.PrototypeFileModel;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author: 超哥呦
 * @Description: 原型功能持久层
 * @date: 2021/11/13 23:29
 **/
public interface PrototypeFileMapper extends BaseMapper<PrototypeFileEntity> {

    /**
     * 查询最新的版本号
     * @param code
     * @return
     */
    @Select("select version from cgycms_prototype_file where code = #{0} order by version desc limit 1")
    Integer queryNewVersion(String code);

    /**
     * 查询新文件编码是否存在
     * @param fileName
     * @return
     */
    @Select("SELECT count(*) FROM cgycms_prototype_file where file_new_name = #{0}")
    Integer queryNewFileName(String fileName);

    /**
     * 根据项目编码查询所有原型
     * @param projectCode
     * @return
     */
    @Select("select id,original_name originalName,file_new_name fileNewName,preview_url previewUrl,upload_file_type uploadFileType,create_time createTime,version,code,create_by createBy from cgycms_prototype_file where code = #{0} order by version desc  ")
    List<PrototypeFileModel> selectProjectList(String projectCode);

    /**
     * 根据原型编码查询原型信息
     * @param code
     * @return
     */
    @Select("select id,original_name originalName,file_new_name fileNewName,preview_url previewUrl,upload_file_type uploadFileType,create_time createTime,version,code from cgycms_prototype_file where file_new_name = #{0}  ")
    PrototypeFileModel selectProjectByFileNewName(String code);

    @Select("select preview_url from cgycms_prototype_file where code = #{0} order by  version desc limit 1 ")
    String getProjectNewUri(String code);
}

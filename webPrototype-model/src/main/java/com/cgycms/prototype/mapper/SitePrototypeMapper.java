package com.cgycms.prototype.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cgycms.prototype.entity.SiteEntity;

/**
 * @author: 超哥呦
 * @Description: 站点配置持久层
 * @date: 2021/11/13 23:29
 **/
public interface SitePrototypeMapper extends BaseMapper<SiteEntity> {
}

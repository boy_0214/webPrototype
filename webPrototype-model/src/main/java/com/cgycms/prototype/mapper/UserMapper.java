package com.cgycms.prototype.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cgycms.prototype.entity.LoginUserEntity;
import com.cgycms.prototype.model.UserModel;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author: 超哥呦
 * @Description: 用户信息持久层
 * @date: 2021/11/27 21:50
 **/
public interface UserMapper extends BaseMapper<LoginUserEntity> {

    /**
     * 查询用户信息
     * @param username
     * @return
     */
    @Select("select * from cgycms_users where login_name=#{0} and state = 0 ")
    LoginUserEntity selectUserByLogin(String username);

    /**
     * 查询用户信息
     * @param username
     * @return
     */
    @Select("select count(*) from cgycms_users where login_name=#{0}")
    Integer selectUserByLoginNameCount(String username);

    @Select("select * from cgycms_users ")
    List<UserModel> queryUserList();
}

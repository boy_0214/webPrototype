package com.cgycms.prototype.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cgycms.prototype.entity.SiteEntity;
import com.cgycms.prototype.entity.UserProjectEntity;
import com.cgycms.prototype.model.UserProjectModel;
import org.apache.ibatis.annotations.Select;

/**
 * @author: 超哥呦
 * @Description: 用户项目信息持久层
 * @date: 2021/11/13 23:29
 **/
public interface UserProjectManagerMapper extends BaseMapper<UserProjectEntity> {

    @Select("select * from cgycms_project where project_code = #{0} ")
    UserProjectModel getProjectDataByCode(String code);

    @Select("select count(*) from cgycms_project where project_name = #{0}")
    int selectbyProjectNameCount(String projectName);
}

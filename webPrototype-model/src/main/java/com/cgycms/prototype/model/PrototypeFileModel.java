package com.cgycms.prototype.model;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: 超哥呦
 * @Description: 原型记录实体
 * @date: 2021/11/25 22:18
 **/
@ToString
@Data
public class PrototypeFileModel extends Model<PrototypeFileModel> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    private Long id;

    /**
     * 上传时文件名称
     */
    @ApiModelProperty(value = "原始文件名称")
    private String originalName;

    /**
     * 新的文件名称
     */
    @ApiModelProperty(value = "新的文件名称")
    private String fileNewName;


    /**
     * 预览地址
     */
    @ApiModelProperty(value = "访问地址")
        private String previewUrl;

    /**
     * 上传时文件类型
     */
    @ApiModelProperty(value = "文件类型")
    private String uploadFileType;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "上传时间")
    private Date createTime;

    /**
     * 版本号
     */
    @ApiModelProperty(value = "版本号")
    private Integer version;

    @ApiModelProperty(value = "上传人id")
    private String createBy;

    @ApiModelProperty(value = "上传人名称")
    private String createByName;

    @ApiModelProperty(value = "项目编码")
    private String code;
}

package com.cgycms.prototype.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: 超哥呦
 * @Description: 上传原型对象
 * @date: 2021/11/18 20:50
 **/
@Data
public class ProjectUploadModel implements Serializable {

    private static final long serialVersionUID = 1L;

    private String projectCode;

}

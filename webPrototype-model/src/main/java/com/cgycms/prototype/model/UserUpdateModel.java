package com.cgycms.prototype.model;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName : UserUpdateModel
 * @Description : 用户修改昵称密码实体
 * @Author : 超哥呦
 * @Date: 2021-12-20 17:10
 */
@Api(value = "项目详情信息对象")
@Data
public class UserUpdateModel implements Serializable {

    @ApiModelProperty("用户ID")
    private Long id;

    @ApiModelProperty("要修改的新密码")
    private String password;

    @ApiModelProperty("要修改的昵称")
    private String nickName;

}

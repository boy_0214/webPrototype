package com.cgycms.prototype.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName UserRegisterModel
 * @Description 用户注册信息实体
 * @Author 超哥呦
 * @Date 2021/12/14 21:53
 **/
@Data
public class UserRegisterModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("用户名称")
    private String nickName;

    @ApiModelProperty("登录账号")
    private String loginName;

    @ApiModelProperty("登录密码")
    private String password;

    @ApiModelProperty("登录状态 0有效 1锁定")
    private Integer state;

}

package com.cgycms.prototype.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @ClassName UserProjectDetailModel
 * @Description 项目详情信息
 * @Author 超哥呦
 * @Date 2021/11/25  22:17
 **/
@Api(value = "项目详情信息对象")
@Data
public class UserProjectDetailModel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 项目名称
     */
    @ApiModelProperty(value = "ID")
    private Long id;

    /**
     * 项目名称
     */
    @ApiModelProperty(value = "项目名称")
    private String projectName;


    /**
     * 项目唯一编码
     */
    @ApiModelProperty(value = "项目编码")
    private String projectCode;

    /**
     * 描述
     */
    @ApiModelProperty(value = "项目描述")
    private String projectDesc;

    /**
     * 是否公开
     */
    @ApiModelProperty(value = "是否公开")
    private Integer toOpen;


    /**
     * 最新原型版本
     */
    @ApiModelProperty(value = "最新原型版本")
    private Integer projectVersion;

    /**
     * 访问密码
     */
    @ApiModelProperty(value = "访问密码")
    private String projectPassword;

    /**
     * 是否匿名访问 0可以 1输入密码
     */
    @ApiModelProperty(value = "匿名访问")
    private Integer passType;

    /**
     * 预览地址
     */
    @ApiModelProperty(value = "原型访问地址")
    private String previewUrl;

    /**
     * 创建人id
     */
    @ApiModelProperty(value = "创建人名称")
    private String createByName;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    @ApiModelProperty(value = "原型数据列表")
    private List<PrototypeFileModel> detailList;

}

package com.cgycms.prototype.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName UserModel
 * @Description 用户信息
 * @Author 超哥呦
 * @Date 2021/11/27 21:53
 **/
@Data
public class UserModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("用户ID")
    private Long id;

    @ApiModelProperty("用户名称")
    private String nickName;

    @ApiModelProperty("登录账号")
    private String loginName;

    @ApiModelProperty("登录密码")
    private String password;

    @ApiModelProperty("登录状态 0有效 1锁定")
    private Integer state;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("用户登录token")
    private String token;

    @ApiModelProperty("token有效期")
    private Long tokenTime;

}

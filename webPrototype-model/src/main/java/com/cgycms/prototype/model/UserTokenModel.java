package com.cgycms.prototype.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName UserTokenModel
 * @Description 用户登录实体
 * @Author 超哥呦
 * @Date 2021/12/14 21:00
 **/
@Data
public class UserTokenModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("用户ID")
    private Long id;

    @ApiModelProperty("用户名称")
    private String nickName;

    @ApiModelProperty("登录账号")
    private String loginName;

    @ApiModelProperty("用户登录token")
    private String token;

    @ApiModelProperty("token有效期")
    private Long tokenTime;

    @ApiModelProperty("超级管理员标识")
    private Boolean superAdmin;

}

package com.cgycms.prototype.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * @ClassName Knife4jConfiguration
 * @Description 接口文档配置类
 * @Author 超哥呦
 * @Date 2021/11/25  21:27
 **/
@Configuration
@EnableSwagger2WebMvc
public class Knife4jConfiguration {

    @Bean(value = "defaultApi2")
    public Docket defaultApi2() {
        Docket docket=new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .title("原型预览存储平台接口文档")
                        .description("# 原型预览存储平台接口文档")
                        .termsOfServiceUrl("http://www.cgycms.com/")
                        .contact(new Contact("超哥呦","http://www.cgycms.com/","boy_0214@sina.com"))
                        .version("1.0")
                        .build())
                //分组名称
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.cgycms.prototype.controller"))
                .paths(PathSelectors.any())
                .build();
        return docket;
    }
}

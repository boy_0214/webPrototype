package com.cgycms.prototype;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * @author: 超哥呦
 * @Description: 启动类
 * @date: 2021/11/14 23:29
 **/
@SpringBootApplication
@MapperScan("com.cgycms.prototype.mapper")
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

}

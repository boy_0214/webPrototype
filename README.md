# 原型WEB预览平台     WebPrototype 

### 交流群

有很多人后台私信，邮件；平时很少登录查看，特此创建一个群，方便大家交流。
![输入图片说明](%E5%BC%80%E6%BA%90%E4%BA%A4%E6%B5%81%E7%BE%A4%E8%81%8A%E4%BA%8C%E7%BB%B4%E7%A0%81.png)

### 常见问题请先查看
https://gitee.com/boy_0214/webPrototype/wikis

### 原型WEB预览平台简介

本项目是用于原型文件进行在线预览，不需要PM每次更新原型发压缩包给RD。
也不需要RD安装各类插件才能查看；降低沟通成本，文件在线预览


### 使用方式
##### 查看项目中帮助，会有详细的使用说明

    1.创建自己的项目
    2.上传HTML压缩包
    3.复制项目预览地址
    4.公开项目其他人也可在线查阅

### 使用到的技术

    1.springboot 2.3.7
    2.vue + antd 
    3.mysql
    4.mybatis-plus
    5.sa-token
    6.hutool
    ...


### 运行使用
    jdk8、mysql5.6以上
    linux、windows操作系统
    推荐磁盘500G以上（主要看压缩包大小及原型数量）
    不支持集群部署
    访问http://host:8081
    默认账号密码： admin/admin123


### 配置说明
    1.安装完数据库后，需要创建数据库并执行SQL文件，然后根据下面语句进行新增即可。
        执行两条SQL：
        INSERT INTO cgycms_site(`id`, `site_name`, `disk_name`, `zip_out_name`, `preview_name`, `role_switch`, `host_name`, `oss_switch`, `data_version`, `site_logo`) VALUES (1, 'WEB文件预览存储平台', '/data/prototype/datazip', '/data/prototype/datahtml', '/disk', 0, '', 0, 0, '');        
        INSERT INTO cgycms_users(`id`, `nick_name`, `login_name`, `password`, `state`) VALUES (1, 'admin', 'admin', 'f5866c4a4d6014ecced47960c2e3d07f', 0);
    2.如果https使用nginx反向代理
    
    3.默认数据库：jdbc:mysql://127.0.0.1:3306/web_prototype   root/123
    
    4.修改数据库配置：application.properties/spring.datasource
    
    5.修改访问端口：application.properties/server.port 默认8081

    6.配置的存储路径及预览路径最后不要带‘/’ 正确： /disk  D:/prototype


### Linux发行版使用方式
    1.安装jdk1.8,mysql5.6以上 自行百度
    2.下载好jar包放到指定目录  例如放在/data/prototype
    3.创建原型文件存放目录 mkdir /data/prototype/datazip   mkdir /data/prototype/datahtml 
    4.打开数据库运行sql语句  INSERT INTO cgycms_site(`id`, `site_name`, `disk_name`, `zip_out_name`, `preview_name`, `role_switch`, `host_name`, `oss_switch`, `data_version`, `site_logo`) VALUES (1, 'WEB文件预览存储平台', '/data/prototype/datazip', '/data/prototype/datahtml', '/disk', 0, '', 0, 0, '');INSERT INTO cgycms_users(`id`, `nick_name`, `login_name`, `password`, `state`) VALUES (1, 'admin', 'admin', 'f5866c4a4d6014ecced47960c2e3d07f', 0);
    5.运行程序 nohup java -jar /data/prototype/webprototype.jar --server.port=指定端口  --spring.datasource.url=数据库地址 --spring.datasource.username=数据库账号 --spring.datasource.password=数据库密码  >> web.log & 
    6.访问ip:端口即可
    
### 前端项目代码
    https://gitee.com/boy_0214/webprototype-web.git

# 项目推荐

### 其他开源项目推荐

* websql网页sql管理工具 https://gitee.com/boy_0214/websql


* webdeploy网页发布管理工具 https://gitee.com/boy_0214/webdeploy


* weblog日志收集工具

    https://gitee.com/boy_0214/weblog





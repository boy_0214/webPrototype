package com.cgycms.prototype.util;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.cgycms.prototype.SitePrototypeService;
import com.cgycms.prototype.common.BizException;
import com.cgycms.prototype.entity.SiteEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @ClassName ProjectUtil
 * @Description 项目工具类
 * @Author 超哥呦
 * @Date 2021/11/30  21:59
 **/
public class ProjectUtil {

    private static Logger logger = LoggerFactory.getLogger(ProjectUtil.class);

    /**
     * 根据uri 解析原型文件编码
     *
     * @param uri
     * @return
     */
    public static String getProjectCode(String uri) {
        if (StringUtils.isEmpty(uri)) {
            logger.error("必填参数为空,URI为空！");
            throw new BizException("解析项目编码失败,uri为空！");
        }
        SitePrototypeService site = SpringUtil.getBean(SitePrototypeService.class);
        SiteEntity siteEntity = site.querySite();
        String codePrefix = StrUtil.removePrefix(uri, siteEntity.getPreviewName() + "/");
        codePrefix.split("/");
        List<String> codeSubList = StrUtil.split(codePrefix, "/");
        String code = codeSubList.get(0);
        if (StringUtils.isEmpty(code)) {
            throw new BizException("解析项目编码失败,未找到编码！");
        }
        return code;
    }
}

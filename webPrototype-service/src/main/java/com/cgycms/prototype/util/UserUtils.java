package com.cgycms.prototype.util;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.ObjectUtil;
import com.cgycms.prototype.model.UserTokenModel;

/**
 * @ClassName : UserUtils
 * @Description : 获取用户信息工具类
 * @Author : 超哥呦
 * @Date: 2021-12-14 17:46
 */
public class UserUtils {

    /**
     * 获取当前登录人信息
     *
     * @return
     */
    public static UserTokenModel getUser() {
        UserTokenModel userModel = (UserTokenModel) StpUtil.getTokenSession().get("user");
        return userModel;
    }

    /**
     * 获取当前登录人是否超级管理员
     *
     * @return
     */
    public static boolean isAdmin() {
        UserTokenModel user = getUser();
        if (user.getId() == 1L) {
            return true;
        }
        return false;
    }

    /**
     * 强制下线
     * @param userId userId 可选，如果为NULL则当前登录人踢出
     * @return
     */
    public static boolean kicking(Long userId) {
        if (ObjectUtil.isNull(userId)) {
            StpUtil.kickout(getUser().getId());
        } else {
            StpUtil.kickout(userId);
        }
        return true;
    }
}

package com.cgycms.prototype;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cgycms.prototype.entity.PrototypeFileEntity;
import com.cgycms.prototype.model.PrototypeFileModel;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @author: 超哥呦
 * @Description: 原型功能服务接口
 * @date: 2021/11/18 20:21
 **/
public interface ProjectManagerService extends IService<PrototypeFileEntity> {

    /**
     * 上传文件
     *
     * @param code          项目编号
     * @param multipartFile 文件
     * @param projectId     项目的主键id
     * @return
     */
    Object projectUpload(String code, Long projectId, MultipartFile multipartFile) throws Exception;

    /**
     * 查询原型列表
     * @param projectCode 项目编码
     * @return
     */
    List<PrototypeFileModel> selectProjectList(String projectCode);

    /**
     * 删除原型文件
     * @param id
     * @return
     */
    Object projectDelete(Long id);

    /**
     * 根据项目编码，批量删除所有文件
     * @param code
     * @return
     */
    Boolean projectDeleteByCode(String code);


    /**
     * 查询原型地址，根据fie_new_name字段
     * @param code
     * @return
     */
    PrototypeFileModel selectProjectByFileNewName(String code);

    /**
     * 根据项目编码，查询最新的原型地址
     * @param code
     * @return
     */
    String getProjectNewUri(String code);
}

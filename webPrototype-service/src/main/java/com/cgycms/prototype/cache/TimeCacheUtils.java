package com.cgycms.prototype.cache;

import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.TimedCache;
import com.cgycms.prototype.constant.ServiceCacheKeyConstant;

/**
 * @author: 超哥呦
 * @Description: 超时缓存配置类
 * @date: 2021/11/13 23:43
 **/
public class TimeCacheUtils {

    private static TimedCache<String, Object> timedCache = CacheUtil.newTimedCache(ServiceCacheKeyConstant.CACHE_TIMEOUT);

    static {
        timedCache.schedulePrune(ServiceCacheKeyConstant.CACHE_TIMEOUT_DELAY);
    }

    public static Object get(String key) {
        return timedCache.get(key);
    }

    public static void put(String key, Object object) {
        put(key, object, ServiceCacheKeyConstant.CACHE_TIMEOUT);
    }

    public static void put(String key, Object object, Long time) {
        timedCache.put(key, object, time);
    }

    public static void remove(String key){
         timedCache.remove(key);
    }

    public static void clear(){
        timedCache.clear();
    }
}

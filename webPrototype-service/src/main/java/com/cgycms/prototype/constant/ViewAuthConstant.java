package com.cgycms.prototype.constant;

/**
 * @ClassName ViewAuthConstant
 * @Description 访问常量类
 * @Author 超哥呦
 * @Date 2021/11/30  0:31
 **/
public class ViewAuthConstant {


    /**
     * 访问密码常量key
     */
    public final static String ACCESS_PASSWORD_KEY= "viewAuth_";


}

package com.cgycms.prototype.constant;

/**
 * @author: 超哥呦
 * @Description: 缓存常量标识
 * @date: 2021/11/13 23:40
 **/
public class ServiceCacheKeyConstant {

    /**
     * 站点配置缓存
     */
    public final static String SITE_CACHE_KEY = "web:prototype:entity:site";

    /**
     * 原型项目缓存
     */
    public final static String USER_PROJECT_KEY = "web:prototype:entity:project:";

    /**
     * 所有用户信息
     */
    public final static String SYS_USERS_ALL = "web:prototype:users:all";

    /**
     * 原型详情信息缓存
     */
    public final static String PROJECT_ID_KEY = "web:prototype:project:id:";

    /**
     * 默认缓存时间(1天)
     */
    public final static Long CACHE_TIMEOUT = 86400000L;

    /**
     * 定时检查缓存失效时间（10分钟）
     */
    public final static Long CACHE_TIMEOUT_DELAY = 600000L;


}

package com.cgycms.prototype;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cgycms.prototype.entity.UserProjectEntity;
import com.cgycms.prototype.model.UserProjectDetailModel;
import com.cgycms.prototype.model.UserProjectModel;

import java.util.List;


/**
 * @author: 超哥呦
 * @Description: 用户项目管理服务口
 * @date: 2021/11/24 23:49
 **/
public interface UserProjectManagerService extends IService<UserProjectEntity> {

    Object createProject(UserProjectModel model);

    List<UserProjectModel> queryProjectList(String name, Integer type);

    UserProjectDetailModel queryProjectById(Long id);

    Object projectDelete(Long id);

    /**
     * 根据请求路径，查询项目信息
     * @param uri
     * @return
     */
    UserProjectModel getProjectDataByUri(String uri);

    /**
     * 根据项目ID修改项目详情信息
     * @param model
     * @return
     */
    Object updateProject(UserProjectModel model);
}

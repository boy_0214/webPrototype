package com.cgycms.prototype;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cgycms.prototype.entity.SiteEntity;

/**
 * @author: 超哥呦
 * @Description: 站点配置服务
 * @date: 2021/11/13 23:30
 **/
public interface SitePrototypeService extends IService<SiteEntity> {

    /**
     * 查询站点信息
     *
     * @return
     */
    SiteEntity querySite();
}

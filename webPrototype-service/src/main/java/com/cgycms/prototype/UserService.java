package com.cgycms.prototype;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cgycms.prototype.entity.LoginUserEntity;
import com.cgycms.prototype.model.UserModel;
import com.cgycms.prototype.model.UserTokenModel;

import java.util.List;
import java.util.Map;

/**
 * @author: 超哥呦
 * @Description: 用户信息服务接口
 * @date: 2021/11/24 23:49
 **/
public interface UserService extends IService<LoginUserEntity> {

    /**
     * 登录
     *
     * @param username
     * @param password
     * @return
     */
    UserTokenModel doLogin(String username, String password);

    /**
     * 查询相同登录账号是否存在
     *
     * @param loginName
     * @return
     */
    Integer selectUserByLoginNameCount(String loginName);

    /**
     * 查询用户信息
     *
     * @return Map<userId, UserModel>
     */
    Map<Long, UserModel> queryUserMap();

    /**
     * 查询用户列表
     * @return
     */
    List<UserTokenModel> queryUserList();

    /**
     * 批量删除用户信息
     * @param ids
     * @return
     */
    Object deleteBatchUser(List<Long> ids);
}

package com.cgycms.prototype.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cgycms.prototype.SitePrototypeService;
import com.cgycms.prototype.cache.TimeCacheUtils;
import com.cgycms.prototype.constant.ServiceCacheKeyConstant;
import com.cgycms.prototype.entity.SiteEntity;
import com.cgycms.prototype.mapper.SitePrototypeMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author: 超哥呦
 * @Description: 站点配置服务实现
 * @date: 2021/11/13 23:31
 **/
@Service
public class SitePrototypeServiceImpl extends ServiceImpl<SitePrototypeMapper, SiteEntity> implements SitePrototypeService {

    @Resource
    private SitePrototypeMapper sitePrototypeMapper;

    @Override
    public SiteEntity querySite() {
        Object site = TimeCacheUtils.get(ServiceCacheKeyConstant.SITE_CACHE_KEY);
        if (ObjectUtil.isNull(site)) {
            site = sitePrototypeMapper.selectOne(null);
            if (ObjectUtil.isNotNull(site)) {
                TimeCacheUtils.put(ServiceCacheKeyConstant.SITE_CACHE_KEY, site);
            }
        }
        return (SiteEntity) site;
    }
}
